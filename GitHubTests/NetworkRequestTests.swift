//
//  NetworkRequestTests.swift
//  GitHubTests
//
//  Created by marcosreboucas.com on 21/10/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import XCTest
@testable import GitHub

class NetworkRequestTests: XCTestCase {
    func testRequestExecution() {
        let session = PartialMockSession()
        let request = MockRequest(session: session)
        request.execute { _ in
            XCTAssertEqual(request.data, PartialMockDataTask.dummyData)
            XCTAssertEqual(request.response, PartialMockDataTask.dummyResponse)
        }
    }
}

// MARK: PartialMockDataTask
class PartialMockDataTask: URLSessionDataTask {
    static let dummyData = Data()
    static let dummyResponse = URLResponse(url: URL(string: "www.test.com")!, mimeType: nil, expectedContentLength: 0, textEncodingName: nil)
    let completionHandler: (Data?, URLResponse?, Error?) -> Void
    
    init(completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        self.completionHandler = completionHandler
    }
    
    override func resume() {
        completionHandler(PartialMockDataTask.dummyData, PartialMockDataTask.dummyResponse, nil)
    }
}

// MARK: PartialMockSession
class PartialMockSession: URLSession {
    override func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return PartialMockDataTask(completionHandler: completionHandler)
    }
}

// MARK: MockRequest
class MockRequest: NetworkRequest {
    typealias ModelType = Int
    let session: URLSession
    var task: URLSessionDataTask?
    var data: Data?
    var response: URLResponse?
    
    init(session: URLSession) {
        self.session = session
    }
    
    func deserialize(_ data: Data?, response: URLResponse?) -> Int? {
        self.data = data
        self.response = response
        return nil
    }
    
    var urlRequest: URLRequest {
        return URLRequest(url: URL(string: "test.com")!)
    }
}
