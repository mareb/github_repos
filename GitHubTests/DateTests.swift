//
//  DateTests.swift
//  GitHubTests
//
//  Created by marcosreboucas.com on 25/08/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import XCTest
@testable import GitHub

class DateTests: XCTestCase {
    func testFormat() {
        let date = Date(string: "03/11/2017 12:00:00")!
        XCTAssertEqual(date.dateText, "Updated on November 3, 2017")
    }
}


