//
//  URLTests.swift
//  GitHubTests
//
//  Created by marcosreboucas.com on 09/07/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import XCTest
@testable import GitHub

class URLTests: XCTestCase {
    
    func testInitFromTemplate() {
        let template = "https://api.github.com/users/octocat/starred{/owner}{/repo}"
        guard let url = URL(template: template) else {
            XCTFail("The initialization failed")
            return
        }
        XCTAssertEqual(url.absoluteString, "https://api.github.com/users/octocat/starred")
    }
    
}
