//
//  EditProfileViewController.swift
//  GitHub
//
//  Created by marcosreboucas.com on 17/09/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController, Stateful, UsersCoordinated, Networked, MainCoordinated {
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var saveButton: UIBarButtonItem!
    @IBOutlet private var scrollingController: TableViewScrollingController!
    private var dataSource: EditProfileTableViewDataSource?
    private var keyboardObservers: [NSObjectProtocol] = []
    private var transducer: Transducer<State, Trigger> = Transducer(state: .ready)
    private var error: Error?
    var stateController: StateController?
    weak var usersCoordinator: UsersFlowCoordinator?
    weak var mainFlowCoordinator: MainFlowCoordinator?
    var networkController: NetworkController?
    var user: FetchableValue<User>?
    
    var avatar: UIImage? {
        didSet {
            avatar.map { dataSource?.set(avatar: $0) }
            tableView.reloadData()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        transducer.addEntry(for: .editing, withAction: disableSaving)
        transducer.addExit(for: .editing, withAction: enableSaving)
        transducer.addEntry(for: .submitting, withAction: disableUI)
        transducer.addExit(for: .submitting, withAction: enableUI)
        transducer.addTransition(withOrigin: .ready, trigger: .dataChanged, guard: { !self.isDataValid }, destination: .editing)
        transducer.addTransition(withOrigin: .ready, trigger: .save, destination: .submitting, action: submit)
        transducer.addTransition(withOrigin: .editing, trigger: .dataChanged, guard: { self.isDataValid }, destination: .ready)
        transducer.addTransition(withOrigin: .submitting, trigger: .userSubmitted, destination: .finished, action: finishUpdate)
        transducer.addTransition(withOrigin: .submitting, trigger: .errorOcurred, guard: { self.isClientAuthenticated }, destination: .ready, action: showAlert)
        transducer.addTransition(withOrigin: .submitting, trigger: .errorOcurred, guard: { !self.isClientAuthenticated}, destination: .waiting, action: authenticate)
        transducer.addTransition(withOrigin: .waiting, trigger: .didAppear, destination: .submitting, action: submit)
    }
    
    @IBAction func save() {
        transducer.fire(.save)
    }
}

extension EditProfileViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        registerForKeyboardNotifications()
        guard let user: User = stateController?.user else {
            return
        }
        let dataSource = EditProfileTableViewDataSource(user: user)
        self.dataSource = dataSource
        tableView.dataSource = dataSource
        tableView.delegate = self
        tableView.reloadData()
    }
}

// MARK: Keyboard Notifications
private extension EditProfileViewController {
    func registerForKeyboardNotifications() {
        let defaultCenter = NotificationCenter.default
        let keyboardDidShowObserver = defaultCenter.addObserver(forName: Notification.Name.UIKeyboardDidShow, object: nil, queue: nil, using:
        { [weak self] notification in
            self?.scrollingController.keyboardFrame = notification.keyboardFrame
        })
        let keyboardDidHideObserver = defaultCenter.addObserver(forName: Notification.Name.UIKeyboardDidHide, object: nil, queue: nil, using: {
            [weak self] notification in
            self?.scrollingController.keyboardFrame = nil
        })
        keyboardObservers = [keyboardDidShowObserver, keyboardDidHideObserver]
    }
}

extension Notification {
    var keyboardFrame: CGRect {
        return (userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue ?? CGRect.zero
    }
}

// MARK: UITableViewDelegate
extension EditProfileViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch cell {
        case let cell as AvatarInputCell: cell.delegate = self
        case let cell as DetailInputCell: cell.delegate = self
        case let cell as BioInputCell: cell.delegate = self
        default: break
        }
    }
}

//// MARK: ScrollingDelegate
extension EditProfileViewController: ScrollingDelegate {
    func activeViewDidChange(_ view: UIView?) {
        guard let activeView = view else {
            scrollingController.activeViewFrame = nil
            return
        }
        let frame = activeView.convert(activeView.bounds, to: tableView)
        scrollingController.activeViewFrame = frame
    }
}

// MARK: AvatarInputCellDelegate
extension EditProfileViewController: AvatarInputCellDelegate {
    func photoCellDidEditPhoto(_ cell: AvatarInputCell) {
        usersCoordinator?.viewControllerDidEditPhoto(self)
    }
}

// MARK: CoordinatorDelegate
extension EditProfileViewController: UsersCoordinatorDelegate {
    func coordinatorDidPick(image: UIImage) {
        dataSource?.set(avatar: image)
        tableView.reloadData()
    }
}

// MARK: DetailInputCellDelegate
extension EditProfileViewController: DetailInputCellDelegate {
    func inputCell(_ cell: DetailInputCell, didChange text: String) {
        guard let dataSource = dataSource,
            let index = tableView.indexPath(for: cell)?.row else {
                return
        }
        dataSource.set(text: text, at: index)
        saveButton.isEnabled = dataSource.isDataValid
        tableView.performBatchUpdates({
            cell.configure(with: dataSource.row(at: index))
        }, completion: nil)
        transducer.fire(.dataChanged)
    }
    
    func inputCellDidReturn(_ cell: DetailInputCell) {
        tableView.showCursorInCell(after: cell)
    }
}

// MARK: BioInputCellDelegate
extension EditProfileViewController: BioInputCellDelegate {
    func bioCell(_ cell: BioInputCell, didChange text: String) {
        UIView.setAnimationsEnabled(false)
        tableView.performBatchUpdates({
            guard let index = tableView.indexPath(for: cell)?.row else {
                return
        }
            dataSource?.set(text: text, at: index)
        }, completion: { _ in
            UIView.setAnimationsEnabled(true)
        })
    }
}

// MARK: EditProfileViewController.Row
extension EditProfileViewController {
    enum Row {
        case avatar(UIImage)
        case name(String)
        case blog(String)
        case company(String)
        case location(String)
        case bio(String)
        
        var cellType: UITableViewCell.Type {
            switch self {
            case .avatar: return AvatarInputCell.self
            case .name, .blog, .company, .location: return DetailInputCell.self
            case .bio: return BioInputCell.self
            }
        }
        
        var separator: UITableViewCell.SeparatorType {
            switch self {
            case .avatar: return .none
            case .bio, .blog, .company, .location, .name: return .insetted(24.0)
            }
        }
        
        var label: String {
            switch self {
            case .name: return "Name"
            case .blog: return "Blog"
            case .company: return "Company"
            case .location: return "Location"
            case .bio: return "Bio"
            case .avatar:
                assertionFailure("The avatar has no label")
                return ""
                
            }
        }
        
        var placeHolder: String {
            switch self {
            case .name: return "Name or Nickname"
            case .blog: return "example.com"
            case .company: return "Company Name"
            case .location: return "City"
            case .bio: return "Tell us a bit about yourself"
            case .avatar:
                assertionFailure("The avatar has no placeholder")
                return ""
            }
        }
        
        var errorMessage: String {
            switch self {
            case .name, .company, .location: return ""
            case .blog: return "The URL is not valid"
            case .bio:
                assertionFailure("The bio has no error message")
                return ""
            case .avatar:
                assertionFailure("The avatar has no error message")
                return ""
            }
        }
    }
}

// MARK: State machine State and Trigger
extension EditProfileViewController {
    enum State: TransducerState {
        case ready
        case editing
        case submitting
        case finished
        case waiting
    }
    
    enum Trigger: TransducerTrigger {
        typealias State = EditProfileViewController.State
        case dataChanged
        case save
        case userSubmitted
        case errorOcurred
        case didAppear
    }
}

// MARK: State machine guards
private extension EditProfileViewController {
    var isDataValid: Bool {
        return dataSource?.isDataValid ?? false
    }
    
    var isClientAuthenticated: Bool {
        return networkController?.isClientAuthenticated ?? false
    }
}

// MARK: State machine ENTRY and EXIT actions
private extension EditProfileViewController {
    func disableSaving() {
        navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    func enableSaving() {
        navigationItem.rightBarButtonItem?.isEnabled = true
    }
    
    func disableUI() {
        navigationItem.leftBarButtonItem?.isEnabled = false
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
        activityIndicator.startAnimating()
        let button = UIBarButtonItem(customView: activityIndicator)
        navigationItem.rightBarButtonItem = button
        disableSaving()
    }
    
    func enableUI() {
        navigationItem.leftBarButtonItem?.isEnabled = true
        navigationItem.rightBarButtonItem = saveButton
        enableSaving()
    }
}

// MARK: State TRANSACTION actions
private extension EditProfileViewController {
    func submit() {
        guard let dataSource = dataSource else {
            return
        }
        networkController?.submit(value: dataSource.userUpdate, toURL: GitHubEndpoint.updateUserURL, withCompletion: { [weak self] (result: Result<User>) in
            do {
                let updatedUser = try result.get()
                self?.user?.update(newValue: updatedUser)
                self?.transducer.fire(.userSubmitted)
            } catch {
                self?.error = error
                self?.transducer.fire(.errorOcurred)
            }
        })
    }
    
    func showAlert() {
        (error as? NetworkError).map { mainFlowCoordinator?.showAlert(for: $0) }
    }
    
    func authenticate() {
        mainFlowCoordinator?.authenticate()
    }
    
    func finishUpdate() {
        usersCoordinator?.editProfileViewControllerDidSaveUser(self)
    }
}
