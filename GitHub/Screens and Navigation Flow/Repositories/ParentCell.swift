//
//  ParentCell.swift
//  GitHub
//
//  Created by marcosreboucas.com on 26/08/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import UIKit

class ParentCell: UITableViewCell {
    @IBOutlet private weak var label: UILabel!
    
    var parentName: String = "" {
        didSet {
            label.text = parentName
        }
    }
}
