//
//  RepositoriesViewController.swift
//  GitHub
//
//  Created by marcosreboucas.com on 25/08/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import UIKit

class RepositoriesViewController: UIViewController, Stateful, MainCoordinated {
    @IBOutlet private weak var tableView: UITableView!
    private var dataSource: RepositoriesTableViewDataSource?
    var stateController: StateController?
    var mainFlowCoordinator: MainFlowCoordinator?
}

// MARK: RepositoriesViewController
extension RepositoriesViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let repositories: [Repository] = stateController?.repositories else {
            return
        }
        let dataSource = RepositoriesTableViewDataSource(repositories: repositories)
        self.dataSource = dataSource
        tableView.dataSource = dataSource
        tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        mainFlowCoordinator?.configure(viewController: segue.destination)
    }
}
