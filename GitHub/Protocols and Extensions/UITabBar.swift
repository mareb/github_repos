//
//  UITabBar.swift
//  GitHub
//
//  Created by marcosreboucas.com on 11/08/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import UIKit

extension UITabBar {
    static func setCustomAppearance() {
        UITabBar.appearance().unselectedItemTintColor = .cork
    }
}
