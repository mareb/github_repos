//
//  AvatarCell.swift
//  GitHub
//
//  Created by marcosreboucas.com on 07/09/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import UIKit

class AvatarCell: UITableViewCell {
    @IBOutlet private weak var avatarImageView: UIImageView!
    
    var avatar = UIImage() {
        didSet {
            avatarImageView.image = avatar
        }
    }
}
