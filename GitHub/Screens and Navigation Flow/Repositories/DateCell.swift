//
//  DateCell.swift
//  GitHub
//
//  Created by marcosreboucas.com on 26/08/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import UIKit

class DateCell: UITableViewCell {
    @IBOutlet private weak var dateLabel: UILabel!
    
    var viewModel = ViewModel() {
        didSet {
            dateLabel.text = viewModel.dateText
        }
    }

}

extension DateCell {
    struct ViewModel {
        var date = Date()
        
        var dateText: String {
            return date.dateText
        }
    }
}
