//
//  CountersCell.swift
//  GitHub
//
//  Created by marcosreboucas.com on 26/08/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import UIKit

class CountersCell: UITableViewCell {
    @IBOutlet private weak var starsLabel: UILabel!
    @IBOutlet private weak var forksLabel: UILabel!
    
    var viewModel = ViewModel() {
        didSet {
            starsLabel.text = viewModel.starsText
            forksLabel.text = viewModel.forksText
        }
    }
}

extension CountersCell {
    struct ViewModel {
        var starsCount = 0
        var forksCount = 0
        
        var starsText: String {
            return "\(starsCount)"
        }
        
        var forksText: String {
            return "\(forksCount)"
        }
    }
}
