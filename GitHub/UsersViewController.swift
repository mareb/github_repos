//
//  UsersViewController.swift
//  GitHub
//
//  Created by marcosreboucas.com on 11/04/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import UIKit

class UsersViewController: UIViewController, Stateful, MainCoordinated {
    @IBOutlet weak private var tableView: UITableView!
    private var dataSource: UsersTableViewDataSource?
    var stateController: StateController?
    var mainFlowCoordinator: MainFlowCoordinator?
}

extension UsersViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let users: [User] = stateController?.users else {
            return
        }
        let dataSource = UsersTableViewDataSource(users: users)
        self.dataSource = dataSource
        tableView.dataSource = dataSource
        tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        mainFlowCoordinator?.configure(viewController: segue.destination)
    }
}

