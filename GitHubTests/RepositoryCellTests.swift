//
//  RepositoryCellTests.swift
//  GitHubTests
//
//  Created by marcosreboucas.com on 25/08/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import Foundation

import XCTest
@testable import GitHub

class RepositoryCellTests: XCTestCase {
    func testViewModel() {
        var viewModel = RepositoryCell.ViewModel()
        viewModel.starsCount = 2
        viewModel.forksCount = 3
        XCTAssertEqual(viewModel.starsText, "2")
        XCTAssertEqual(viewModel.forksText, "3")
    }
}
