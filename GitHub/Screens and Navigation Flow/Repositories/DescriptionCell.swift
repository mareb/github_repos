//
//  DescriptionCell.swift
//  GitHub
//
//  Created by marcosreboucas.com on 26/08/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import UIKit

class DescriptionCell: UITableViewCell {
    @IBOutlet private weak var label: UILabel!
    
    var descriptionText: String = "" {
        didSet {
            label.text = descriptionText
        }
    }

}
