//
//  RepositoryTests.swift
//  GitHubTests
//
//  Created by marcosreboucas.com on 22/07/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import XCTest
@testable import GitHub

class RepositoryTests: XCTestCase {
    
    func testRepositoryDecoding() {
        let repository: Repository = openJsonFile(with: "Repository")!
        XCTAssertEqual(repository.id.value, 1296269)
        XCTAssertEqual(repository.name, "Hello-World")
        XCTAssertEqual(repository.isFork, true)
        XCTAssertEqual(repository.forksCount, 9)
        XCTAssertEqual(repository.owner.id.value, 1)
        XCTAssertEqual(repository.description, "This your first repo!")
        XCTAssertNil(repository.language)
        XCTAssertNil(repository.parent)
    }
}
