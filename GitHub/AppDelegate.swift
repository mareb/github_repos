//
//  AppDelegate.swift
//  GitHub
//
//  Created by marcosreboucas.com on 11/04/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var coordinator: MainFlowCoordinator?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UINavigationBar.setCustomAppearance()
        UITabBar.setCustomAppearance()
        if let initialViewController = window?.rootViewController as? MainTabBarController {
            coordinator = MainFlowCoordinator(mainViewController: initialViewController)
        }
        return true
    }

}

