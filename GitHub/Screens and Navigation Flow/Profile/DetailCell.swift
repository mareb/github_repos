//
//  DetailCell.swift
//  GitHub
//
//  Created by marcosreboucas.com on 07/09/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import UIKit

class DetailCell: UITableViewCell {
    @IBOutlet private weak var iconImageView: UIImageView!
    @IBOutlet private weak var label: UILabel!
    
    var viewModel: ViewModel = ViewModel() {
        didSet {
            iconImageView.image = viewModel.icon
            label.text = viewModel.text
            label.textColor = viewModel.active ? .mediumCarmine : .cork
        }
    }
}

// MARK: - ViewModel
extension DetailCell {
    struct ViewModel {
        var icon: UIImage = UIImage()
        var text: String = ""
        var active: Bool = false
    }
}
