//
//  Date.swift
//  GitHub
//
//  Created by Marcos Rebouças on 22/08/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import Foundation

extension Date {
    var dateText: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .none
        let formatted = formatter.string(from: self)
        return "Updated on " + formatted
    }
}
