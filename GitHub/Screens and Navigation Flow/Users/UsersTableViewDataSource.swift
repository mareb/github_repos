//
//  UsersTableViewDataSource.swift
//  GitHub
//
//  Created by marcosreboucas.com on 13/08/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import UIKit

class UsersTableViewDataSource: NSObject {
    internal var dataOrganizer: ArrayDataSourceOrganizer<User>
    internal var viewModelCache: [IndexPath : UserCell.ViewModel] = [:]
    
    init(users: [User]) {
        dataOrganizer = ArrayDataSourceOrganizer(items: users)
        super.init()
    }
}

// MARK: ArrayTableViewDataSource
extension UsersTableViewDataSource: ArrayTableViewDataSource {
    func viewModel(for value: User) -> UserCell.ViewModel {
        return UserCell.ViewModel(user: value)
    }
    
    func configure(cell: UserCell, with viewModel: UserCell.ViewModel) {
        cell.viewModel = viewModel
    }
}

// MARK: ArrayTableViewDataSource
extension UsersTableViewDataSource: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowsCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cell(from: tableView, for: indexPath)
    }
}

//private extension UsersTableViewDataSource {
//    struct DataOrganizer {
//        let users: [User]
//        
//        var rowsCount: Int {
//            return users.count
//        }
//        
//        subscript (indexPath: IndexPath) -> User {
//            return users[indexPath.row]
//        }
//    }
//}

// MARK - UserCell.ViewModel
extension UserCell.ViewModel {
    init(user: User) {
        username = user.login
        avatar = #imageLiteral( resourceName: "Avatar")
        guard case let .fetched(details) = user.details.value else {
            return
        }
        name = details.name ?? ""
        bio = details.bio ?? ""
        location = details.location ?? ""
        
    }
}

// MARK - Loader
struct Loader {
    static func loadDataFromJSONFile<Model: Decodable>(withName name: String) -> Model? {
        guard let url = Bundle.main.url(forResource: name, withExtension: "json"),
            let data = try? Data(contentsOf: url) else {
                return nil
        }
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        guard let model = try? decoder.decode(Model.self, from: data) else {
            return nil
        }
        return model
    }
}

















