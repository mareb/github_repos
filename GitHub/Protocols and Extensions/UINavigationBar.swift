//
//  UINavigationBar.swift
//  GitHub
//
//  Created by marcosreboucas.com on 11/08/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import UIKit

extension UINavigationBar {
    
    static func setCustomAppearance() {
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().barTintColor = .mediumCarmine
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
}
