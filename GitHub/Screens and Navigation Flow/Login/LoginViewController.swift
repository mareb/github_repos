//
//  LoginViewController.swift
//  GitHub
//
//  Created by marcosreboucas.com on 26/10/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, Networked, LoginCoordinated {
    
    @IBOutlet private weak var loginButton: UIButton!
    @IBOutlet private weak var authenticatingView: UIStackView!
    @IBOutlet private weak var safariLogoutView: UIStackView!
    
    private let state = UUID().description
    var presentedAfterLogout = false
    
    var networkController: NetworkController?
    weak var loginCoordinator: LoginFlowCoordinator?
    
    var isAuthenticating: Bool = false {
        didSet {
            loginButton.isEnabled = !isAuthenticating
            authenticatingView.isHidden = !isAuthenticating
        }
    }
    
    func performAuthorization(with authorizationCode: String) {
        isAuthenticating = true
        networkController?.authenticateWith(authorizationCode: authorizationCode, state: state) { [weak self] _ in
            self?.loginCoordinator?.loginViewControllerDidFinishAuthorization()
        }
    }
    
    @IBAction func login(_ sender: Any) {
        loginCoordinator?.loginViewController(self, didStartAuthorizationWithState: state)
    }
    
    @IBAction func signOut(_ sender: Any) {
        loginCoordinator?.loginViewControllerDidSignOut()
    }
}

// MARK: UIViewController
extension LoginViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        isAuthenticating = false
        safariLogoutView.isHidden = !presentedAfterLogout
    }
}
