//
//  MainTabBarControllerTransducer.swift
//  GitHub
//
//  Created by marcosreboucas.com on 17/11/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import Foundation

protocol MainTabBarControllerTransducerDelegate: AnyObject {
    func fetchRoot()
    func authenticate()
    func showAlert()
    func finishLaunch()
}

class MainTabBarControllerTransducer {
    weak var delegate: MainTabBarControllerTransducerDelegate?
    
    var state = State.empty {
        didSet {
            switch state {
            case .waiting: delegate?.authenticate()
            case .loading: delegate?.fetchRoot()
            default: break
            }
        }
    }
    
    func fire(_ trigger: Trigger) {
        switch (state, trigger) {
        case (.empty, .load(authenticated: false)): state = .waiting
        case (.empty, .load(authenticated: true)): state = .loading
        case (.waiting, .load): state = .loading
        case (.loading, .errorOcurred(authenticated: false)): state = .waiting
        case (.loading, .errorOcurred(authenticated: true)):
            state = .empty
            delegate?.showAlert()
        case (.loading, .rootFetched):
            state = .loaded
            delegate?.finishLaunch()
        default: break
        }
    }
}

extension MainTabBarControllerTransducer {
    enum State {
        case empty
        case waiting
        case loading
        case loaded
    }
    
    enum Trigger {
        case load(authenticated: Bool)
        case errorOcurred(authenticated: Bool)
        case rootFetched
    }
    // In this State Machine, the Guards are represented by these associated types in enum Trigger: (authenticated: Bool)
}
