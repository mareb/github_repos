//
//  OwnerCell.swift
//  GitHub
//
//  Created by marcosreboucas.com on 26/08/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import UIKit

class OwnerCell: UITableViewCell {
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var usernameLabel: UILabel!
    
    var viewModel = ViewModel() {
        didSet {
            avatarImageView.image = viewModel.avatar
            nameLabel.text = viewModel.name
            usernameLabel.text = viewModel.username
        }
    }

}   

extension OwnerCell {
    struct ViewModel {
        var avatar = UIImage()
        var name = ""
        var username = ""
    }
}
