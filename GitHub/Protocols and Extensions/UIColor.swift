//
//  UIColor.swift
//  GitHub
//
//  Created by marcosreboucas.com on 11/08/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import UIKit

extension UIColor {
    static let alto = UIColor(named: "Alto")!
    static let cork = UIColor(named: "Cork")!
    static let dustyGray = UIColor(named: "Dusty Grey")!
    static let mediumCarmine = UIColor(named: "Medium Carmine")!
    static let roseWhite = UIColor(named: "Rose White")!
}
