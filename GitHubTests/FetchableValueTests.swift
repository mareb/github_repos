//
//  FetchableValueTests.swift
//  GitHubTests
//
//  Created by marcosreboucas.com on 10/07/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import XCTest
@testable import GitHub

class FetchableValueTests: XCTestCase {
    func testCorruptedDataError() {
        let json = "{ \"avatar_url\": \"\" }".data(using: .utf8)!
        XCTAssertThrowsError(try JSONDecoder().decode(TestStruct.self, from: json))
    }
}

private extension FetchableValueTests {
    
    struct TestStruct: Decodable {
        
        let testProperty: FetchableValue<Bool>
        
        enum CodingKeys: String, CodingKey {
            case avatar_url
        }
        
        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            testProperty = try container.decode(FetchableValue.self, forKey: .avatar_url)
        }
        
    }
    
}
