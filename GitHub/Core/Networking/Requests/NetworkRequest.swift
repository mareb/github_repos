//
//  NetworkRequest.swift
//  GitHub
//
//  Created by marcosreboucas.com on 16/10/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import Foundation

protocol NetworkRequest: class {
    associatedtype ModelType
    var urlRequest: URLRequest { get }
    var session: URLSession { get }
    var task: URLSessionDataTask? { get set }
    func deserialize(_ data: Data?, response: HTTPURLResponse) throws -> ModelType
}

extension NetworkRequest {
    func execute(withCompletion completion: @escaping (Result<ModelType>) -> Void) {
        task = session.dataTask(with: urlRequest) { [weak self] (data, response, error) in
            guard let strongSelf = self else {
                return
            }
            let result = Result { () throws -> ModelType in
                try error?.toNetworkError()
                guard let response = response as? HTTPURLResponse else {
                    throw NetworkError.unrecoverable
                }
                try response.validate()
                return try strongSelf.deserialize(data, response: response)
            }
            completion(result)
        }
        task?.resume()
    }
}

// MARK: Validable
protocol Validable {
    func validade(_ response: HTTPURLResponse) throws
}

// MARK: - JSONDataRequest
protocol JSONDataRequest: Validable, NetworkRequest where ModelType: Decodable {}

extension JSONDataRequest {
    func deserialize(_ data: Data?, response: HTTPURLResponse) throws -> ModelType {
        guard let data = data else {
            throw NetworkError.unrecoverable
        }
        try validade(response)
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        do { return try decoder.decode(ModelType.self, from: data) }
        catch { throw NetworkError.unrecoverable }
    }
}

// MARK: - HTTPStatusRequest
protocol HTTPStatusCodeRequest: Validable, NetworkRequest {}

extension HTTPStatusCodeRequest {
    func deserialize(_ data: Data?, response: HTTPURLResponse) throws -> Bool {
        try validade(response)
        switch response.statusCode {
        case 204: return true
        case 404: return false
        default:
            assertionFailure("Unexpected Status Code")
            throw NetworkError.unrecoverable
        }
    }
}
