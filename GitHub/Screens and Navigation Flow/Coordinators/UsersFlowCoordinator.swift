//
//  UsersFlowCoordinator.swift
//  GitHub
//
//  Created by marcosreboucas.com on 12/10/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import UIKit
import MessageUI
import Photos

protocol UsersCoordinatorDelegate: class {
    func coordinatorDidPick(image: UIImage)
}

class UsersFlowCoordinator: NSObject {
    weak var parent: Coordinator?
    weak var delegate: UsersCoordinatorDelegate?
    
    func profileViewControllerDidSelectUsers(_ viewController: ProfileViewController) {
        viewController.performSegue(withIdentifier: ProfileViewControllerSegues.showUsers, sender: nil)
    }
    
    func profileViewControllerDidSelectRepositories(_ viewController: ProfileViewController) {
        viewController.performSegue(withIdentifier: ProfileViewControllerSegues.showRepositories, sender: nil)
    }
    
    func profileViewController(_ viewController: ProfileViewController, didSelectEmail email: String) {
        let mailViewController = MFMailComposeViewController()
        mailViewController.setToRecipients([email])
        viewController.show(mailViewController, sender: nil)
    }
    
    func viewControllerDidEditPhoto(_ viewController: UIViewController & UsersCoordinatorDelegate) {
        delegate = viewController
        
        if PHPhotoLibrary.authorizationStatus() != PHAuthorizationStatus.authorized {
            PHPhotoLibrary.requestAuthorization({ (status: PHAuthorizationStatus) in
                
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                viewController.present(imagePicker, animated: true, completion: nil)
            })
        }
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        viewController.present(imagePicker, animated: true, completion: nil)
        
    }
    
    func editProfileViewControllerDidSaveUser(_ viewController: EditProfileViewController) {
        viewController.performSegue(withIdentifier: EditProfileViewControllerSegues.dismissAfterSave, sender: nil)
    }
}

extension UsersFlowCoordinator {
    struct ProfileViewControllerSegues {
        static let showUsers = "ShowUsers"
        static let showRepositories = "ShowRepositories"
    }
    
    struct EditProfileViewControllerSegues {
        static let dismissAfterSave = "DismissAfterSave"
    }
}

extension UsersFlowCoordinator: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            delegate?.coordinatorDidPick(image: image)
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

extension UsersFlowCoordinator: Coordinator {
    func configure(viewController: UIViewController) {
        parent?.configure(viewController: viewController)
    }
}
