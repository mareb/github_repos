//
//  GitHubEndpoint.swift
//  GitHub
//
//  Created by marcosreboucas.com on 19/10/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import Foundation

struct GitHubEndpoint {
    struct FieldNames {
        static let state = "state"
        static let clientID = "client_id"
        static let clientSecret = "client_secret"
        static let authorizationCode = "code"
        static let page = "page"
        static let scope = "scope"
    }
    
    static let clientID = "4723aa7b4e3deb784142"
    static let clientSecret = "e6e8ee7c7a8b1c06f8fb3aa13e8544646bc8bc5b"
    static let scope = "user"
    static let authorizationCallbackURLScheme = "mygithub://"
    static let accessTokenURL = URL(string: "https://github.com/login/oauth/access_token")!
    static let authorizationURL = URL(string: "https://github.com/login/oauth/authorize")!
    static let serverURL = URL(string: "https://github.com")!
    static let signOutURL = URL(string: "https://github.com/logout")!
    static let apiRootURL = URL(string: "https://api.github.com")!
    
    static var updateUserURL: URL {
        return apiRootURL.appendingPathComponent("/user")
    }
    
    static func authorizationUrl(with state: String) -> URL {
        var urlComponents = URLComponents(url: GitHubEndpoint.authorizationURL, resolvingAgainstBaseURL: false)!
        urlComponents.queryItems = [
            URLQueryItem(name: FieldNames.clientID, value: GitHubEndpoint.clientID),
            URLQueryItem(name: FieldNames.state, value: state),
            URLQueryItem(name: FieldNames.scope, value: GitHubEndpoint.scope)
        ]
        return urlComponents.url!
    }
}

extension URL {
    var authorizationCode: String? {
        guard absoluteString.contains(GitHubEndpoint.authorizationCallbackURLScheme) else {
            return nil
        }
        guard let components = URLComponents(url: self, resolvingAgainstBaseURL: false),
        let queryItems = components.queryItems else {
            return nil
        }
        for queryItem in queryItems {
            if queryItem.name == GitHubEndpoint.FieldNames.authorizationCode {
                return queryItem.value
            }
        }
        return nil
    }
}

// MARK: - GitHubRoot
struct GitHubRoot: Decodable {
    enum CodingKeys: String, CodingKey {
        case user = "current_user_url"
        case repositories = "repository_url"
        case stars = "starred_url"
        case followers = "followers_url"
        case following = "following_url"
    }
    
    let user: FetchableValue<User>
    let repositories: FetchableValue<[Repository]>
    let stars: FetchableValue<[Repository]>
    let followers: FetchableValue<[User]>
    let following: FetchableValue<[User]>
}
