//
//  AvatarInputCell.swift
//  GitHub
//
//  Created by marcosreboucas.com on 17/09/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import UIKit

protocol AvatarInputCellDelegate: class {
    func photoCellDidEditPhoto(_ cell: AvatarInputCell)
}

class AvatarInputCell: UITableViewCell {
    @IBOutlet private weak var avatarImageView: UIImageView!
    weak var delegate: AvatarInputCellDelegate?
    
    var avatar = UIImage() {
        didSet {
            avatarImageView.image = avatar
        }
    }
    
    @IBAction func changeAvatar(_ sender: Any) {
        delegate?.photoCellDidEditPhoto(self)
    }
    
}
