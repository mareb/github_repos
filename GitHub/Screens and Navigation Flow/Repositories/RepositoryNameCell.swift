//
//  RepositoryNameCell.swift
//  GitHub
//
//  Created by marcosreboucas.com on 26/08/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import UIKit

class RepositoryNameCell: UITableViewCell {
    @IBOutlet private weak var label: UILabel!
    
    var name: String = "" {
        didSet {
            label.text = name
        }
    }

}
