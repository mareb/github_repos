//
//  UIStoryboardSegue.swift
//  GitHub
//
//  Created by marcosreboucas.com on 03/11/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import UIKit

extension UIStoryboardSegue {
    func forward(_ user: FetchableValue<User>?) {
        func forward(_ user: FetchableValue<User>?, to viewController: UIViewController) {
            (viewController as? EditProfileViewController)?.user = user
            (viewController as? UINavigationController)?.viewControllers.first.map { forward(user, to: $0) }
        }
        forward(user, to: destination)
    }
    
    func readUser() -> FetchableValue<User>? {
        return (source as? EditProfileViewController)?.user
    }
}
