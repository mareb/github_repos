//
//  Transducer.swift
//  GitHub
//
//  Created by marcosreboucas.com on 19/11/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import Foundation

protocol TransducerState: Hashable {}

protocol TransducerTrigger: Hashable {
    associatedtype State: TransducerState
}

class Transducer<State, Trigger: TransducerTrigger> where Trigger.State == State {
    private var state: State
    private var transitions: [Condition<State, Trigger> : [Effect<State>]] = [:]
    private var entries: [State : Action] = [:]
    private var exits: [State : Action] = [:]
    
    init(state: State) {
        self.state = state
    }
    
    func addTransition(withOrigin origin: State, trigger: Trigger, guard: @escaping Guard = { true }, destination: State, action: @escaping Action = {}) {
        let condition = Condition(origin: origin, trigger: trigger)
        let effect = Effect(guard: `guard`, destination: destination, action: action)
        add(condition, with: effect)
    }
    
    func add(_ condition: Condition<State, Trigger>, with effect: Effect<State>) {
        if let existingEffects = transitions[condition], !existingEffects.isEmpty {
            transitions[condition] = existingEffects + [effect]
        } else {
            transitions[condition] = [effect]
        }
    }
    
    func addEntry(for state: State, withAction action: @escaping Action) {
        entries[state] = action
    }
    
    func addExit(for state: State, withAction action: @escaping Action) {
        exits[state] = action
    }
    
    func fire(_ trigger: Trigger) {
        guard let effects = transitions[Condition(origin: state, trigger: trigger)] else {
            return
        }
        for effect in effects {
            guard effect.guard() else {
                continue
            }
            state = effect.destination
            effect.action()
            break
        }
    }
}

extension Transducer {
    typealias Guard = () -> Bool
    typealias Action = () -> Void
    
    struct Condition<State, Trigger: TransducerTrigger>: Hashable where Trigger.State == State {
        let origin: State
        let trigger: Trigger
    }
    
    struct Effect<State: TransducerState> {
        let `guard`: Guard
        let destination: State
        let action: Action
    }
}
