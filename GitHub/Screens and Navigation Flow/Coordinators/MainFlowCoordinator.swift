//
//  MainFlowCoordinator.swift
//  GitHub
//
//  Created by marcosreboucas.com on 10/10/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import UIKit
import MessageUI
import SafariServices

// MARK: Protocols
protocol Coordinator: class {
    func configure(viewController: UIViewController)
}

protocol MainCoordinated: class {
    var mainFlowCoordinator: MainFlowCoordinator? { get set }
}

protocol UsersCoordinated: class {
    var usersCoordinator: UsersFlowCoordinator? { get set }
}

protocol Stateful: class {
    var stateController: StateController? { get set }
}

protocol Networked: class {
    var networkController: NetworkController? { get set }
}

protocol LoginCoordinated: class {
    var loginCoordinator: LoginFlowCoordinator? { get set }
}

class MainFlowCoordinator: NSObject, SFSafariViewControllerDelegate {
    let stateController = StateController()
    let keychainController = KeychainController()
    let cachingController = CachingController()
    let mainTabBarController: MainTabBarController
    let usersFlowCoordinator = UsersFlowCoordinator()
    let loginFlowCoordinator = LoginFlowCoordinator()
    
    init(mainViewController: MainTabBarController) {
        self.mainTabBarController = mainViewController
        super.init()
        usersFlowCoordinator.parent = self
        loginFlowCoordinator.parent = self
        configure(viewController: mainViewController)
    }
    
    func viewController(_ viewController: UIViewController, didSelectURL url: URL) {
        let safariViewController = SFSafariViewController(url: url)
        safariViewController.delegate = self
        viewController.present(safariViewController, animated: true, completion: nil)
    }
    
    func mainViewController(_ viewController: MainTabBarController, didLoadGitHubRoot root: GitHubRoot) {
        viewController.viewControllers?.forEach({ child in
            guard let navigationController = child as? UINavigationController,
                let viewController = navigationController.viewControllers.first else {
                    return
            }
            (viewController as? ProfileViewController)?.user = root.user
        })
    }
    
    func authenticate() {
    loginFlowCoordinator.mainViewControllerRequiresAuthentication(mainTabBarController, isAppLaunch: false)
    }
    
    func showAlert(for error: NetworkError, withActionTitle title: String = "OK", completion: @escaping () -> Void = {}) {
        assert(error != .unauthorized)
        let alertController = UIAlertController(title: error.errorDescription, message: error.failureReason, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: title, style: .default, handler: { _ in completion() })
        alertController.addAction(dismissAction)
        mainTabBarController.show(alertController, sender: nil)
    }
}

extension MainFlowCoordinator: Coordinator {
    func configure(viewController: UIViewController) {
        (viewController as? MainCoordinated)?.mainFlowCoordinator = self
        (viewController as? UsersCoordinated)?.usersCoordinator = usersFlowCoordinator
        (viewController as? LoginCoordinated)?.loginCoordinator = loginFlowCoordinator
        (viewController as? Stateful)?.stateController = stateController
        (viewController as? Networked)?.networkController = NetworkController(keychainController: keychainController, cachingController: cachingController)
        if let tabBarController = viewController as? UITabBarController {
            tabBarController.viewControllers?.forEach(configure(viewController:))
        }
        if let navigationController = viewController as? UINavigationController,
            let rootViewController = navigationController.viewControllers.first {
            configure(viewController: rootViewController)
        }
    }
}
