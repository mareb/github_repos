//
//  Paddable.swift
//  GitHub
//
//  Created by marcosreboucas.com on 07/09/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import UIKit

protocol Paddable {
    var verticalPadding: CGFloat { get set }
    var horizontalPadding: CGFloat { get set }
}

extension Paddable {
    func pad(_ size: CGSize) -> CGSize {
        return CGSize(width: size.width + horizontalPadding, height: size.height + verticalPadding)
    }
}

@IBDesignable
class PaddedLabel: UILabel, Paddable {
    @IBInspectable var verticalPadding: CGFloat = 0
    @IBInspectable var horizontalPadding: CGFloat = 0
    
    override var intrinsicContentSize: CGSize {
        return pad(super.intrinsicContentSize)
    }
}

@IBDesignable
class PaddedButton: UIButton, Paddable {
    @IBInspectable var verticalPadding: CGFloat = 0
    @IBInspectable var horizontalPadding: CGFloat = 0
    
    override var intrinsicContentSize: CGSize {
        return pad(super.intrinsicContentSize)
    }
}
