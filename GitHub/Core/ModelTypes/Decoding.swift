//
//  Decoding.swift
//  GitHub
//
//  Created by marcosreboucas.com on 18/06/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import Foundation

extension URL {
    init?(template: String) {
        let regex = "\\{.*\\}"
        let cleanedString = template.replacingOccurrences(of: regex, with: "", options: .regularExpression)
        self.init(string: cleanedString)
    }
}

extension KeyedDecodingContainer {
    func value<T>(forKey key: KeyedDecodingContainer.Key) throws -> T where T: Decodable {
        return try decode(T.self, forKey: key)
    }
}
