//
//  ImageRequest.swift
//  GitHub
//
//  Created by marcosreboucas.com on 17/10/18.
//  Copyright © 2018 marcosreboucas.com. All rights reserved.
//

import Foundation
import UIKit

class ImageRequest {
    let url: URL
    let session: URLSession
    var task: URLSessionDataTask?
    
    init(url: URL, session: URLSession) {
        self.url = url
        self.session = session
    }
}

// MARK: NetworkRequest
extension ImageRequest: NetworkRequest {
    var urlRequest: URLRequest {
        return URLRequest(url: url)
    }
    
    func deserialize(_ data: Data?, response: HTTPURLResponse) throws -> UIImage {
        guard let data = data, let image = UIImage(data: data) else {
            throw NetworkError.unrecoverable
        }
        return image
    }
    
}
